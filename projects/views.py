from django.shortcuts import get_object_or_404, redirect, render
from projects.forms import ProjectForm
from projects.models import Project
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required
def list_projects(request):
    # Retrieve all projects owned by the current user
    projects = Project.objects.filter(owner=request.user)
    # Render the list of projects to the project_list template
    return render(
        request, "projects/project_list.html", {"projects": projects}
    )


@login_required
def project_detail(request, id):
    # Retrieve a project by its id or return 404 if not found
    project = get_object_or_404(Project, id=id)
    # Render the project details to the project_detail template
    return render(
        request, "projects/project_detail.html", {"project": project}
    )


@login_required
def create_project(request):
    if request.method == "POST":
        # Process the form data if the form is submitted
        form = ProjectForm(request.POST)
        if form.is_valid():
            # Save the form data but do not commit to database yet
            project = form.save(commit=False)
            # Set the project owner to the current user
            project.owner = request.user
            # Save the project to the database
            project.save()
            return redirect("list_projects")
    else:
        # Display an empty form for a new project
        form = ProjectForm()

    return render(
        request, "projects/create/create_project.html", {"form": form}
    )


@login_required
def edit_project(request, id):
    instance = get_object_or_404(Project, id=id)
    if request.method == "POST":
        # Process the form data if the form is submitted
        form = ProjectForm(request.POST, instance=instance)
        if form.is_valid():
            # Save the updated project
            form.save()
            # Redirect to the list of projects
            return redirect("list_projects")
    else:
        # Pre-fill the form with the project's existing data
        form = ProjectForm(instance=instance)

    # Render the edit project form to the edit_project template
    return render(request, "projects/edit/edit_project.html", {"form": form})


@login_required
def delete_project(request, id):
    project = get_object_or_404(Project, id=id)
    if request.method == "POST":
        # Check if delete action was confirmed
        if "delete" in request.POST:
            # Delete the project
            project.delete()
            return redirect("list_projects")
        elif "cancel" in request.POST:
            return redirect("list_projects")
    else:
        # Retrieve all tasks related to the project for confirmation display
        tasks = project.tasks.all()
        return render(
            request,
            "projects/delete/confirm_delete_project.html",
            {"project": project, "tasks": tasks},
        )

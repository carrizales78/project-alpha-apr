from django.db import models
from django.contrib.auth.models import User

# Define the Project model


class Project(models.Model):
    # Field to store the project's name with a maximum length of 200 characters
    name = models.CharField(max_length=200)

    # Text field to store a longer description of the project
    description = models.TextField()

    # ForeignKey relationship to the User model for project ownership
    # 'on_delete=models.CASCADE' means the project will be deleted if the owner is deleted
    # 'related_name' allows access to projects from the User model
    # 'null=True' allows projects to be without an owner
    owner = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        related_name="projects",
        null=True,
    )

    # String representation of the Project model, returning the project's name
    def __str__(self):
        return self.name

from django.urls import path
from projects.views import (
    create_project,
    delete_project,
    edit_project,
    list_projects,
    project_detail,
)


urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", project_detail, name="show_project"),
    path("create/create/", create_project, name="create_project"),
    path("edit/<int:id>/", edit_project, name="edit_project"),
    path("delete/<int:id>/", delete_project, name="delete_project"),
]

from django.urls import path
from tasks.views import (
    calendar_view,
    create_task,
    delete_task,
    edit_task,
    event_data,
    show_my_tasks,
    task_detail,
    task_detail_modal,
)


urlpatterns = [
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("<int:id>/details/", task_detail, name="task_detail"),
    path("create/create/", create_task, name="create_task"),
    path("edit/<int:id>/", edit_task, name="edit_task"),
    path("delete/<int:id>/", delete_task, name="delete_task"),
    path("calendar/", calendar_view, name="calendar_view"),
    path("calendar/events/", event_data, name="event_data"),
    path('modal/<int:id>/', task_detail_modal, name='task_detail_modal'),
]

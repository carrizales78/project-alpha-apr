from datetime import timedelta
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from tasks.models import Task
from django.http import HttpResponse, HttpResponseForbidden, JsonResponse
from django.template.loader import render_to_string


# Create your views here.
@login_required
def show_my_tasks(request):
    # Query for tasks assigned to the current user and order them
    tasks = Task.objects.filter(assignee=request.user).order_by(
        "project__name", "due_date"
    )
    # Render the tasks in the show_my_tasks template
    return render(request, "tasks/show_my_tasks.html", {"tasks": tasks})


@login_required
def task_detail(request, id):
    # Retrieve the task or return a 404 error if not found or not assigned to the user
    task = get_object_or_404(Task, id=id, assignee=request.user)
    project = task.project

    # Check if the request is a POST request
    if request.method == "POST":
        # Toggle the task's completion status
        task.is_completed = not task.is_completed
        task.save()

        # Redirect to a specified URL after updating the task
        return redirect("show_my_tasks")

    # Render the task detail page for GET requests
    return render(request, "tasks/task_detail.html", {"task": task, "project": project})


@login_required
def task_detail_modal(request, id):
    task = get_object_or_404(Task, id=id, assignee=request.user)
    project = task.project

    # Check if the request is a POST request
    if request.method == "POST":
        # Get the value from the form
        is_completed = request.POST.get('is_completed') == 'True'
        task.is_completed = is_completed
        task.save()

        # Check if the request is an AJAX request
        if request.headers.get("X-Requested-With") == "XMLHttpRequest":
            # Respond with JSON if it's an AJAX request
            return JsonResponse({'status': 'success', 'message': 'Task updated successfully.'})
        else:
            # Redirect for a non-AJAX request
            return redirect("show_my_tasks")

    # Check if the request is an AJAX request for rendering modal
    if request.headers.get("X-Requested-With") == "XMLHttpRequest":
        # Render only a part of the page (modal) for AJAX request
        html = render_to_string(
            "calendar/task_detail_modal.html",
            {"task": task, "project": project},
            request,
        )
        return HttpResponse(html)


@login_required
def create_task(request):
    # Handle form submission
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.owner = request.user
            task.save()
            return redirect("show_my_tasks")
    else:
        # Display an empty form for creating a new task
        form = TaskForm()

    # Render the form in the create_task template
    return render(request, "tasks/create/create_task.html", {"form": form})


@login_required
def edit_task(request, id):
    task = get_object_or_404(Task, id=id)
    # Handle form submission
    if request.method == "POST":
        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            form.save()
            return redirect("show_my_tasks")
    else:
        # Display the form pre-filled with task details
        form = TaskForm(instance=task)

    return render(request, "tasks/edit/edit_task.html", {"form": form})


@login_required
def delete_task(request, id):
    task = get_object_or_404(Task, id=id)
    # Handle form submission
    if request.method == "POST":
        if "delete" in request.POST:
            task.delete()
            return redirect("show_my_tasks")
        elif "cancel" in request.POST:
            return redirect("show_my_tasks")
    else:
        # Render the delete confirmation page
        return render(
            request, "tasks/delete/confirm_delete_task.html", {"task": task}
        )


@login_required
def calendar_view(request):
    # Render the calendar view
    return render(request, "calendar/calendar.html")


@login_required
def event_data(request):
    # Query for tasks assigned to the current user
    tasks = Task.objects.filter(assignee=request.user)

    # Prepare the event data in a format suitable for FullCalendar
    events = [
        {
            "id": task.id,
            "title": task.name,
            "start": task.start_date.date().isoformat(),
            "end": (task.due_date.date() + timedelta(days=1)).isoformat(),
            "allday": True,
            "color": "green" if task.is_completed else "red",
        }
        for task in tasks
    ]
    # Return the events as JSON
    return JsonResponse(events, safe=False)

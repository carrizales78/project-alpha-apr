from django.db import models
from django.contrib.auth.models import User
from projects.models import Project


class Task(models.Model):
    # Field to store the name of the task, with a maximum length of 200 characters
    name = models.CharField(max_length=200)

    # DateTime fields for storing the start and due dates of the task
    start_date = models.DateTimeField()
    due_date = models.DateTimeField()

    # Boolean field to track if the task is completed, defaults to False
    is_completed = models.BooleanField(default=False)

    # ForeignKey relationship to the Project model
    # 'related_name' allows access to tasks from the Project model
    # 'on_delete=models.CASCADE' deletes tasks if the related project is deleted
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    # ForeignKey relationship to the User model, for assigning a user to a task
    # 'null=True' allows tasks to be unassigned
    # 'related_name' allows access to tasks from the User model
    # 'on_delete=models.CASCADE' deletes tasks if the related user is deleted
    assignee = models.ForeignKey(
        User,
        null=True,
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    # String representation of the Task model, returning the task's name
    def __str__(self):
        return self.name
